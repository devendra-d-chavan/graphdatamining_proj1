/*
 * IS2.cpp
 *
 *  Created on: Oct 11, 2013
 *      Author: xczou
 */

#ifndef IS2_CPP_
#define IS2_CPP_

#include <iostream>
#include <vector>
#include <set>
#include <stdint.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
#include <map>
#include <sys/time.h>
#include "weight.cpp"
#include "LA.cpp"

using namespace std;

#define DEBUG 0

void findNeighbors(vector<vector<size_t> > * graph /*IN*/,
		set<size_t> * cluster /*IN*/, set<size_t> * neighbors/*OUT*/) {

	set<size_t>::iterator itr;
	for (itr = cluster->begin(); itr != cluster->end(); ++itr) {
		vector<size_t> neighborList = graph->at(*itr);
		neighbors->insert(neighborList.begin(), neighborList.end());
	}
}

void IS2(set<size_t> *cluster/*IN & OUT*/, vector<vector<size_t> > * graph /*IN*/) {

        size_t cluster_size = cluster->size();
	size_t inner_edges = (cluster_size <= 1) ? 0 : GetNumInnerEdges(graph, cluster);

	float w = (2.0 * inner_edges) / cluster_size;
	float cw = w;
	size_t tmp_edges = inner_edges;
	int increased = 1;
	set<size_t>::iterator itr;
	while (increased) {
		set<size_t> neighbor = *cluster; // copy cluster nodes as initial neighbor node set

		findNeighbors(graph, cluster, &neighbor);

		cw = w;
		set<size_t>::iterator found;
		float new_w = 0;
		tmp_edges = inner_edges;
		for (itr = neighbor.begin(); itr != neighbor.end(); ++itr) {

			new_w = 0;
			found = cluster->find(*itr);
			size_t newInnerEdges = getInnerEdges(*itr, graph, cluster);

			if (found != cluster->end()) { // v in cluster

				new_w = (2.0 * (tmp_edges - newInnerEdges)) / (cluster_size - 1) ;

				 if (new_w > cw ) {
					cw = new_w;
					tmp_edges = (tmp_edges - newInnerEdges);
					cluster->erase(found); // remove it from cluster
				}


			} else {
				new_w = (2.0 * (tmp_edges + newInnerEdges)) / (cluster_size + 1) ;

				 if (new_w > cw ) {
					cw = new_w;
					tmp_edges = tmp_edges + newInnerEdges;
					cluster->insert(*itr); // add it to cluster
				}

			}

		}

		if (cw == w){
			increased = 0;
		}else{
			w = cw;
			inner_edges = tmp_edges;
		}
	}

}

#endif
