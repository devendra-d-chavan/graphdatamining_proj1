/*
 * main.cpp
 *
 *  Created on: Sep 12, 2013
 *      Author: xczou, ddchavan
 */

#include <iostream>
#include <vector>
#include <set>
#include <stdint.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
#include <map>
#include <sys/time.h>
#include "common.cpp"
#include "weight.cpp"
#include "LA.cpp"
#include "IS2.cpp"

using namespace std;

//turn on/off the timing/output
#define TIMING 1

void filter_clusters(vector <set <size_t> > *seeds_C,
                    vector <set <size_t> > *filtered_clusters,
                    size_t min_membership);

int main(int argc, char *argv[]) {

	if (argc < 3) {
		cout << "input graph data file and output file, please " << endl;
		return 1;
	}

	FILE *graph_desc_file = 0;
	graph_desc_file = fopen(argv[1], "r");
	assert (graph_desc_file != 0);

	int num_vertices, num_edges, t;
	t = fscanf(graph_desc_file, "%d", &num_vertices);
	t = fscanf(graph_desc_file, "%d\n", &num_edges);

	vector < vector <size_t> > graph (num_vertices );

#if TIMING
	double load_s =0, load_e = 0, la_s  = 0, la_e = 0, is2_s =  0, is2_e = 0, write_s = 0, write_e = 0;
	load_s= dclock();
#endif
	load_graph(graph_desc_file, &graph, num_edges);

	#if TIMING
	load_e = dclock() - load_s;
#endif

	fclose(graph_desc_file);
	vector <set <size_t> > seeds_C;

#if TIMING
	la_s = dclock();
#endif
	LA(&graph, &seeds_C);
	size_t min_membership = 3;
	vector <set <size_t> > filtered_seeds_C;
	filter_clusters(&seeds_C, &filtered_seeds_C, min_membership);
#if TIMING
	la_e = dclock() - la_s ;
	is2_s = dclock();
#endif

	for(size_t i = 0; i < filtered_seeds_C.size(); ++i ) {
		IS2(&(filtered_seeds_C[i]), &graph);
	}

#if TIMING
	is2_e = dclock() - is2_s;
	write_s = dclock();
#endif
	write_community(argv[2], &filtered_seeds_C);

#if TIMING
	write_e = dclock() - write_s;
	printf("load[%f],LA[%f],IS2[%f],write[%f]\n", load_e,la_e,is2_e,write_e);
#endif

	return 0;
}

// Remove clusters with lower than the minimum number of members
void filter_clusters(vector <set <size_t> > *seeds_C,
                    vector <set <size_t> > *filtered_clusters,
                    size_t min_membership)
{
  for(vector <set <size_t> >::iterator iter = seeds_C->begin();
      iter != seeds_C->end(); ++iter)
  {
      if(iter->size() >= min_membership)
      {
          filtered_clusters->push_back(*iter);
      }
  }
}
