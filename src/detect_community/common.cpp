/*
 * common.cpp
 *
 *  Created on: Oct 11, 2013
 *      Author: xczou
 */
#ifndef COMMON_CPP_
#define COMMON_CPP_


#include <iostream>
#include <vector>
#include <set>
#include <stdint.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
#include <map>
#include <sys/time.h>

using namespace std;


#define OUTPUT 0

double dclock(void) {
	struct timeval tv;
	gettimeofday(&tv, 0);

	return (double) tv.tv_sec + (double) tv.tv_usec * 1e-6;
}


/*
 * build a vector of vector structure to represent the graph, whose data is loaded from input file
 */
void load_graph(FILE *graph_desc_file, vector< vector<size_t> > *graph, int num_edges) {

	int e = 0, p1 = 0, p2=0;

	int r;
	while (e < num_edges) {
		r = fscanf(graph_desc_file, "%d\t%d\n", &p1, &p2);
		graph->at(p1).push_back(p2);
		graph->at(p2).push_back(p1);
		e++;
	}
}

/*
 * writes communities
 */
void write_community(char *output, vector< set<size_t> > *communities){
	FILE *com_file = 0;
	com_file = fopen(output, "w");
	assert (com_file != 0);
	size_t i;
	set<size_t>::iterator itr;
	for(i = 0; i < communities->size(); i++){
		if (communities->at(i).size() > 0){
			for(itr = communities->at(i).begin(); itr != communities->at(i).end() ; itr ++ ){
				fprintf(com_file, "%ld ", *itr);
			}
			fprintf(com_file, "\n");
		}
	}

	fclose(com_file);
}

/*
 * Prints the nodes in the given community
 */
void print_community(const set<size_t> *community) {
	for (set<size_t>::iterator iter = community->begin(); iter
			!= community->end(); ++iter) {
		cout << *iter << " ";
	}
	cout << endl;
}

/*
 * Prints all the nodes for each community
 */
void print_communities(const vector<set<size_t> > *communities) {
        for (vector<set<size_t> >::const_iterator cluster_iter = communities->begin();
             cluster_iter != communities->end(); ++cluster_iter) {

                for (set<size_t>::iterator iter = cluster_iter->begin(); iter
                                != cluster_iter->end(); ++iter) {
                        cout << *iter << " ";
                }

                cout << endl;
        }
        cout << endl;
}


#endif
