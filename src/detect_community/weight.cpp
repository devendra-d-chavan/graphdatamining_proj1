/*
 * weight.cpp
 *
 *  Created on: Oct 9, 2013
 *      Author: xczou, ddchavan
 */
#ifndef WEIGHT_CPP_
#define WEIGHT_CPP_

#include <iostream>
#include <vector>
#include <set>
#include <stdint.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <boost/functional/hash.hpp>

using namespace std;
typedef set<size_t> ClusterSet;
namespace std {
    template <>
        class hash<ClusterSet>{
        public :
            size_t operator()(const ClusterSet &cluster ) const
            {
                return boost::hash_value<ClusterSet>(cluster);
            }
    };
};

// Caches the inner_edges values of a cluster
unordered_map<ClusterSet, size_t> cluster_inner_edges;

bool IsClusterNeighbor(vector<size_t> *neighbors, ClusterSet *cluster,
                       size_t *first_encounter_index);
size_t GetNumInnerEdges(vector<size_t> *neighbors,
                        ClusterSet *cluster, size_t *index);
size_t GetNumInnerEdges(vector<vector<size_t> > *graph, ClusterSet *cluster);


/*
 * This function calculate two clusters's weight, one cluster is `cluster`,
 * another cluster is `cluster` union `node`
 */
bool weightingClusterWithAdditionalNode(vector<vector<size_t> > *graph,
                                        ClusterSet *cluster,
		                        size_t node) {

        size_t inner_edges = 0;
        size_t cluster_size = cluster->size();

        size_t first_encounter_index;
        vector<size_t> neighbors = graph->at(node);

        // Check whether the neighbors of the node are in the cluster
        // i.e. node's outgoing edges are in the cluster,
        //      if yes then get the first neighbor that satisfies this condition;
        //      otherwise return as there is no change in density after adding
        //      the node
        if(!IsClusterNeighbor(&neighbors, cluster, &first_encounter_index))
        {
            return false;
        }

        unordered_map<ClusterSet, size_t>::const_iterator found = cluster_inner_edges.find(*cluster);
        if(found != cluster_inner_edges.end())
        {
            inner_edges = found->second;
        }
        else
        {
            inner_edges = (cluster_size <= 1) ? 0 : GetNumInnerEdges(graph, cluster);
            cluster_inner_edges[*cluster] = inner_edges;
        }

	float old_weight = (2.0 * inner_edges) / cluster_size;

	// `node` is definitely unique , thats because in LA, nodes in C are previous node
	// so we dont have to check the `node` are in `cluster` or not
	inner_edges += GetNumInnerEdges(&neighbors, cluster, &first_encounter_index);

	float new_weight = (2.0 * inner_edges) / (cluster_size + 1);

	return new_weight > old_weight;

}

/*
 * Returns the number of inner edges starting from the first_encounter_index
 * neighbor
 *
 * first_encounter_index is calculated in IsClusterNeighbor()
 * Complexity: O(out_degree(v_i) * log(|C|))
 */
size_t GetNumInnerEdges(vector<size_t> *neighbors, ClusterSet *cluster,
                        size_t *first_encounter_index)
{
        size_t inner_edges = 1;
        vector<size_t>::iterator neighbor_iter = neighbors->begin();

        // Move to the first encountered neighbor that is in the cluster
        advance(neighbor_iter, *first_encounter_index);

        for(;neighbor_iter != neighbors->end(); ++neighbor_iter)
        {
            // Complexity(set.find()) = logarithmic
            if(cluster->find(*neighbor_iter) != cluster->end())
            {
                // Edge with v_i and v_j in cluster is present
                inner_edges++;
            }
        }

        return inner_edges;
}
/*
 * Returns the number of inner edges in the cluster
 */
size_t GetNumInnerEdges(vector<vector<size_t> > *graph, ClusterSet *cluster)
{
        size_t inner_edges = 0;
        size_t first_encounter_index;
        for(ClusterSet::iterator cluster_iter = cluster->begin();
            cluster_iter != cluster->end(); ++cluster_iter)
        {
            vector<size_t> neighbors = graph->at(*cluster_iter);
            if(IsClusterNeighbor(&neighbors, cluster, &first_encounter_index))
            {
                inner_edges += GetNumInnerEdges(&neighbors, cluster, &first_encounter_index);
            }
        }

        // Divide the number of edges in the cluster by 2 since we are counting
        // the number of undirected edges
        return inner_edges/2;
}

/*
 * Determines whether the neighbors of the node are in the cluster and returns
 * the index of the first encountered element
 *
 * Average case optimization (break on first find) of GetNumInnerEdges(neighbors, cluster)
 * Complexity: O(out_degree(v_i) * log(|C|))
 */
bool IsClusterNeighbor(vector<size_t> *neighbors, ClusterSet *cluster,
                        size_t *first_encounter_index)
{
          *first_encounter_index = 0;
          for(vector<size_t>::iterator neighbor_iter = neighbors->begin();
              neighbor_iter != neighbors->end(); ++neighbor_iter)
          {
              (*first_encounter_index)++;
              if(cluster->find(*neighbor_iter) != cluster->end())
              {
                  // Edge with node and cluster, proceed with inner edge calculation
                  return true;
              }
          }

          return false;
}

/*
 * Returns the number of inner edges starting from the first_encounter_index
 * neighbor
 *
 */
size_t getInnerEdges(size_t node, vector<vector<size_t> > *graph, set<size_t> *cluster)
{
        size_t inner_edges = 0;
		vector<size_t> neighbors = graph->at(node);
		size_t i = 0;
		set<size_t>::iterator found ;
		for(i = 0; i < neighbors.size(); i ++ ){
			found = cluster->find(neighbors[i]);
			if (found != cluster->end()){
				inner_edges ++;
			}
		}

        return inner_edges;
}

#endif /* WEIGHT_CPP_ */


