/*
 * weight.cpp
 *
 *  Created on: Oct 12, 2013
 *      Author: ddchavan
 */
#ifndef TIMER_CPP_
#define TIMER_CPP_

#include <sys/time.h>
#include <cstdlib>

class Timer
{
    timeval timer[2];

  public:

    timeval start()
    {
        gettimeofday(&this->timer[0], NULL);
        return this->timer[0];
    }

    timeval stop()
    {
        gettimeofday(&this->timer[1], NULL);
        return this->timer[1];
    }

    double duration() const
    {
        int secs(this->timer[1].tv_sec - this->timer[0].tv_sec);
        int usecs(this->timer[1].tv_usec - this->timer[0].tv_usec);

        if(usecs < 0)
        {
            --secs;
            usecs += 1000000;
        }

        return (secs * 1000 + usecs / 1000.0 + 0.5);
    }
};

#endif /* timer.cpp */
