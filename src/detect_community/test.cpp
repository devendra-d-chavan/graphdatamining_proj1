/*
 * test.cpp
 *
 *  Created on: Oct 11, 2013
 *      Author: xczou
 */

/*
 * mapr_sim.cpp
 *
 *  Created on: Sep 12, 2013
 *      Author: xczou
 */

#include <iostream>
#include <vector>
#include <set>
#include <stdint.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
#include <map>
#include <sys/time.h>
#include "common.cpp"
#include "weight.cpp"
#include "LA.cpp"
#include "IS2.cpp"

using namespace std;

int test_LA(int argc, char * argv[]) {

	if (argc < 2) {
		cout << "input graph data file , please " << endl;
		return 1;
	}

	FILE *graph_desc_file = 0;
	graph_desc_file = fopen(argv[1], "r");
	assert (graph_desc_file != 0);

	int num_vertices, num_edges;
	int t;
	t = fscanf(graph_desc_file, "%d", &num_vertices);
	t = fscanf(graph_desc_file, "%d\n", &num_edges);

	vector<vector<size_t> > graph(num_vertices);

	load_graph(graph_desc_file, &graph, num_edges);

	fclose(graph_desc_file);
	vector<set<size_t> > clusters;
	LA(&graph, &clusters);
	print_communities(&clusters);

	return 0;
}


int test_IS2(int argc, char * argv[]) {
	FILE *graph_desc_file = 0;
	graph_desc_file = fopen(argv[1], "r");
	assert (graph_desc_file != 0);

	int num_vertices, num_edges, t;
	t = fscanf(graph_desc_file, "%d", &num_vertices);
	t = fscanf(graph_desc_file, "%d\n", &num_edges);

	vector<vector<size_t> > graph(num_vertices);

	load_graph(graph_desc_file, &graph, num_edges);

	fclose(graph_desc_file);

	set<size_t> seed;

	seed.insert(10);
	seed.insert(11);

	IS2(&seed, &graph);

	print_community(&seed);

	return 0;
}

int main(int argc, char *argv[]) {
//	test_LA(argc, argv);
	test_IS2(argc, argv);

	return 1;

}
