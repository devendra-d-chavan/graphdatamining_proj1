/*
 * LA.cpp
 *
 *  Created on: Oct 9, 2013
 *      Author: xczou, ddchavan
 */
#ifndef LA_CPP_
#define LA_CPP_

#include <iostream>
#include <vector>
#include <set>
#include <stdint.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
#include <map>
#include <sys/time.h>
#include "weight.cpp"
#include "table.cpp"

using namespace std;

struct sort_pred {
	bool operator()(const std::pair<size_t, double> &left,
			const std::pair<size_t, double> &right) {
		return left.second > right.second;
	}
};

void
GenerateSeedClusters(vector<size_t> *ordered_nodeids,
    vector<vector<size_t> > *graph, vector<set<size_t> > *C);
vector<size_t>
GetPageRank(vector<vector<size_t> > *graph);

/*
 * Initially, *cluster is an empty set of vector,
 * after this function finished, *cluster is the seed cluster set
 */
void LA(vector<vector<size_t> > *graph /*IN*/, vector <set <size_t> > *C /*IN & OUT*/) {

        vector<size_t> ordered_nodeids = GetPageRank(graph);
        GenerateSeedClusters(&ordered_nodeids, graph, C);
}

/*
 * Generates the seed clusters as a part of the first stage
 */
void
GenerateSeedClusters(vector<size_t> *ordered_nodeids,
    vector<vector<size_t> > *graph, vector<set<size_t> > *C)
{
          bool added;
          for (vector<size_t>::iterator node_iter = ordered_nodeids->begin();
               node_iter != ordered_nodeids->end(); ++node_iter) {
                  added = false;
                  for (vector<set<size_t> >::iterator cluster_iter = C->begin();
                       cluster_iter != C->end(); ++cluster_iter) {

                          if (weightingClusterWithAdditionalNode(graph, &(*cluster_iter), *node_iter)){
                             cluster_iter->insert(*node_iter);
                             added = true;
                          }
                  }

                  if (added == false){
                     set<size_t> single_cluster;
                     single_cluster.insert(*node_iter);
                     C->push_back(single_cluster);
                  }
          }
}

/*
 * Orders the vertices by the page rank
 */
vector<size_t>
GetPageRank(vector<vector<size_t> > *graph)
{
          Table t;
          t.read_file(graph);
          t.pagerank();
          vector<double> pr = t.get_pagerank();

          vector<pair<size_t, double> > new_pr;

          for (vector<double>::iterator iter = pr.begin(); iter != pr.end(); ++iter) {
                  new_pr.push_back(make_pair(new_pr.size(), *iter));
          }

          sort(new_pr.begin(), new_pr.end(), sort_pred());

          vector<size_t> ordered_nodeids;
          for (vector<pair<size_t, double> >::iterator iter = new_pr.begin(); iter
                          != new_pr.end(); ++iter) {
                  ordered_nodeids.push_back((*iter).first);
          }

          return ordered_nodeids;
}

#endif
