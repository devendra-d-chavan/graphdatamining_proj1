#!/usr/bin/python

"""
Script to execute community detection program on the graphs and print the 
metrics

Usage:
python GeneratePerformanceMetrics.py <dataset dir path> <output dir path> 
    <metrics scripts dir path> <exec path>

    dataset dir path: Path of the directory containing the graphs and ground 
                        truth communities
    output dir path: Path of the directory that will contain the generated
                        communities
    metrics scripts dir path: Path of the directory containing the 3 metrics 
                        scripts
    exec file path: Path of the community detection executable
"""

def get_rootnames():
    return ['amazon', 'dblp', 'youtube']

def get_graph_sizes():
    return ['small', 'medium', 'large']#, 'original']

def get_num_nodes(graph_filepath):
    num_nodes = 0
    with open(graph_filepath) as f:
        num_nodes = f.readline().strip().split(' ')[0]

    return num_nodes

def get_num_communities(comm_filepath):
    num_comm = 0
    with open(comm_filepath) as f:
        num_comm = len(f.readlines())

    return str(num_comm)

def get_graph_filepath(graph_root_name, graph_size, dir_path):
    import os
    return os.path.join(dir_path, graph_root_name + ".graph." + graph_size) 

def get_comm_filepath(comm_root_name, comm_size, dir_path):
    import os
    return os.path.join(dir_path, comm_root_name + ".comm." + comm_size) 

def get_metrics_filepath(script, dir_path):
    import os
    return os.path.join(dir_path, script)

def run_script(script, arguments):
    from subprocess import call
    
    arguments.insert(0, script)
    arguments.insert(0, 'python')
    call(arguments)

def run_program_4(exec_path, arguments):
    from subprocess import call
    arguments.insert(0, exec_path)
    call(arguments)

def print_metrics(dataset_dir_path, output_dir_path, metrics_scripts_dir_path):
    goodness_script = get_metrics_filepath('GoodnessPerformanceMetrics.py', 
                                                metrics_scripts_dir_path)
    confusion_script = get_metrics_filepath('ConfusionMatrixMetrics.py', 
                                                metrics_scripts_dir_path)
    nmi_script = get_metrics_filepath('NormalizedMutualInformation.py', 
                                                metrics_scripts_dir_path)

    rootnames = get_rootnames()
    graph_sizes = get_graph_sizes()
    for rootname in rootnames:
        for graph_size in graph_sizes:            
            graph_filepath = get_graph_filepath(rootname, graph_size, 
                                                dataset_dir_path)
            comm_gt_filepath = get_comm_filepath(rootname, graph_size, 
                                                dataset_dir_path)
            comm_out_filepath = get_comm_filepath(rootname, graph_size, 
                                                output_dir_path)
            
            num_nodes = get_num_nodes(graph_filepath)
            num_comm_gt = get_num_communities(comm_gt_filepath)
            num_comm_out = get_num_communities(comm_out_filepath)

            arguments = [comm_out_filepath, num_nodes, num_comm_out, graph_filepath]
            print '\n{0} - {1}'.format(rootname, graph_size)
            run_script(goodness_script, arguments)

            arguments = [comm_gt_filepath, comm_out_filepath, num_nodes, 
                            num_comm_gt, num_comm_out]
            run_script(confusion_script, arguments)

            arguments = [comm_gt_filepath, comm_out_filepath, num_nodes, 
                            num_comm_gt, num_comm_out]
            run_script(nmi_script, arguments)

def generate_output_communities(dataset_dir_path, output_dir_path, exec_path):
    import os
    rootnames = get_rootnames()
    graph_sizes = get_graph_sizes()

    for rootname in rootnames:
        for graph_size in graph_sizes:            
            graph_filepath = get_graph_filepath(rootname, graph_size, 
                                                dataset_dir_path)
            comm_out_filepath = get_comm_filepath(rootname, graph_size, 
                                                output_dir_path)

            print '\nGenerating {0} - {1}'.format(rootname, graph_size)
            arguments = [graph_filepath, comm_out_filepath]
            run_program_4(exec_path, arguments)

if __name__ == '__main__':
    import sys

    dataset_dir_path = sys.argv[1]
    output_dir_path = sys.argv[2]
    metrics_scripts_dir_path = sys.argv[3]
    exec_path = sys.argv[4]

    generate_output_communities(dataset_dir_path, output_dir_path, exec_path)    
    print_metrics(dataset_dir_path, output_dir_path, metrics_scripts_dir_path)
