Paper 1 : Efficient Identification of Overlapping Communities implementation
==========================
Team 13: Xiaocheng Zou [xzou2], Kandarp Srivastava [ksrivas], Devendra D. Chavan [ddchavan]
------------------------

Required libraries 
--------------------------
The program requires [g++ 4.7][1] and [boost libraries][2] to be installed on 
the target machine before compilation. They can be installed using the 
following commands

  * In Ubuntu 13.04 - `sudo apt-get install make g++ libboost-dev`
  * In Ubuntu 12.04 (VCL image)
     1. `sudo add-apt-repository -y ppa:ubuntu-toolchain-r/test`
     2. `sudo apt-get update`
     3. `sudo apt-get -y install make g++-4.7`
     4. `sudo ln -s /usr/bin/g++-4.7 /usr/bin/g++`
     5. `cd $HOME`
     6. `wget -c 'http://downloads.sourceforge.net/project/boost/boost/1.54.0/boost_1_54_0.tar.gz'`
     7. `tar -xzf boost_1_54_0.tar.gz`
     8. `cd boost_1_54_0`
     9. `./bootstrap.sh`
     10. `./b2 # Can take some time to complete`
     11. `rm $HOME/boost_1_54_0.tar.gz # Cleanup`

### Installing packages for running the performance metrics
The performance metrics require the python-numpy and python-igraph packages. 
They can be installed using the following commands in 

  * Ubuntu 13.04 `sudo apt-get install python-numpy python-igraph`
  * Ubuntu 12.04 (VCL image)
    1. `sudo apt-get -y install python-numpy`
    2. `sudo add-apt-repository -y ppa:igraph/ppa`
    3. `sudo apt-get update`
    4. `sudo apt-get -y install python-igraph`
    
Compiling the program
--------------------------
The source code is accompanied by a `Makefile` that can be used to compile
the program by running `make` command. This generates the `community` 
executable.

**Note** In case, the program is being built using the steps for Ubuntu 12.04, 
then `INCLUDES` should be updated in the `Makefile` to point to the boost 
install location

For example,

    INCLUDES=-I$(HOME)/boost_1_54_0/

Running the program
--------------------------
The program accepts the graph as the input and generates the communities file.
    ./community <graph file path> <communities file path>

For example,
    ./community amazon.graph amazon_out.graph

Interpreting the results
------------------------
The generated communities are displayed one per line in the output file i.e.
each line of the communities file contains the list of node ids that belong to 
a community.

Sample input and output files
----------------------
To check for correctness the program was run on the following graph generated 
in R. The graph consists of three 5 node cliques with a single edge between 
each of clique to the other cliques.

    library(igraph)
    g <- graph.full(5) %du% graph.full(5) %du% graph.full(5)
    g <- add.edges(g, c(1,6, 1,11, 6, 11))
    plot(g) 

![Test graph][3]

The graph is transformed into a format that is compatible with the program

    15 33
    0 1
    0 2
    0 3
    0 4
    1 2
    1 3
    1 4
    2 3
    2 4
    3 4
    5 6
    5 7
    5 8
    5 9
    6 7
    6 8
    6 9
    7 8
    7 9
    8 9
    10 11
    10 12
    10 13
    10 14
    11 12
    11 13
    11 14
    12 13
    12 14
    13 14
    0 5
    0 10
    5 10

This generates the following communities

    0 5 10 
    0 2 3 4 5 10 
    0 5 7 8 9 
    0 5 10 12 13 14 

In addition, the time taken (in seconds) for each procedure is displayed on
the command line.

    load[0.000042],LA[0.000266],IS2[0.000103],write[0.000208]

Observed performance metrics test results
----------------
These are the results of the tests were carried on the Hadoop CSC 591 VCL 
image.

### Run time
On Thinkpad T530 (Intel® Core™ i5-3210M CPU @ 2.50GHz × 4, 8 GB RAM, 256 GB SSD)

    |Graph  | Runtime (s) | Runtime (h:mm:ss.ms) |
    |-------|------------:|---------------------:|
    |test   |  0.000997   |     0:00:00:0009     | 
    |amazon |  4460.1284  |     1:14:20.1284     |
    |youtube|             |                      |
    |dblp   |  4048.9994  |     1:07:28.9994     |

###Goodness metrics

    |Graph  | Density | Clustering coeff. | Separability | Cohesiveness |
    |-------|--------:|------------------:|-------------:|-------------:|
    |test   |   0.225 |              0.75 |       0.1816 |          0.0 |  
    |amazon |  0.0697 |            0.1568 |       0.0565 |       0.0001 |
    |youtube|         |                   |              |              |
    |dblp   |  0.0643 |            0.2087 |       0.0499 |       0.0005 |

###Confusion metrics

    |Graph  |  TP   |  FP  |  FN  |  TN  | Recall | Precision | F-Measure | Specificity | RAND Index |
    |-------|------:|-----:|-----:|-----:|-------:|----------:|----------:|------------:|-----------:|
    |amazon |       |      |      |      |        |           |           |             |            |  
    |youtube|       |      |      |      |        |           |           |             |            |  
    |dblp   |       |      |      |      |        |           |           |             |            |  

Source code
-----------------
The source code is hosted on a bitbucket repo [here][4]

Credits
----------------
The ranking of the nodes is done in decreasing order of their *PageRank*. The 
implementation is an extension of an [open source C++][5] implementation.

  [1]: http://gcc.gnu.org/gcc-4.7/changes.html
  [2]: http://www.boost.org/doc/libs/1_54_0/libs/libraries.htm
  [3]: http://imageshack.us/a/img822/2871/08zy.png 'Test graph'
  [4]: https://bitbucket.org/devendra-d-chavan/graphdatamining_proj1
  [5]: https://github.com/louridas/pagerank
